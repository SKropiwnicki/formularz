package com.example.vaadindemo.domain;

import java.util.UUID;



public class Weapon
{
	
	private UUID id;
	
	private String name;
	private String description;
	private int damage;
	private double speed;
	private int criticalChance;
	private int reqLevel;
	private double weight;


	private Rarities rarity;
	private WeaponTypes type;

	public Weapon(String name, String description, int damage, double speed, int criticalChance, int reqLevel, double
			weight, Rarities rarity, WeaponTypes type)
	{
		super();
		this.name = name;
		this.description = description;
		this.damage = damage;
		this.speed = speed;
		this.criticalChance = criticalChance;
		this.reqLevel = reqLevel;
		this.weight = weight;
		this.rarity = rarity;
		this.type = type;

	}

	public Weapon() {
	}


	@Override
	public String toString() {
		return "Weapon [Name=" + name +  ", description=" + description
				+", damage=" + damage+", speed=" + speed+", criticalChance=" + criticalChance +
				", reqLevel=" +reqLevel  +", weight="+ weight  +", rarity= "
				+ rarity +", type=" + type +
				"]";
	}


	//=========================================================Get and set==================================

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public int getDamage()
	{
		return damage;
	}

	public void setDamage(int damage)
	{
		this.damage = damage;
	}

	public double getSpeed()
	{
		return speed;
	}

	public void setSpeed(double speed)
	{
		this.speed = speed;
	}

	public int getCriticalChance()
	{
		return criticalChance;
	}

	public void setCriticalChance(int criticalChance)
	{
		this.criticalChance = criticalChance;
	}

	public int getReqLevel()
	{
		return reqLevel;
	}

	public void setReqLevel(int reqLevel)
	{
		this.reqLevel = reqLevel;
	}

	public double getWeight()
	{
		return weight;
	}

	public void setWeight(double weight)
	{
		this.weight = weight;
	}


	public Rarities getRarity()
	{
		return rarity;
	}

	public void setRarity(Rarities rarity)
	{
		this.rarity = rarity;
	}

	public WeaponTypes getType()
	{
		return type;
	}

	public void setType(WeaponTypes type)
	{
		this.type = type;
	}

	//====================================================ENUMS=================================

	public static enum Rarities
	{
		NORMAL,
		MAGIC,
		RARE,
		UNIQUE,
		LEGENDARY;


	}

	public static enum WeaponTypes
	{
		SWORD,
		AXE,
		BOW,
		DAGGER,
		STAFF,
		MACE,
		CROSSBOW;
	}
}

