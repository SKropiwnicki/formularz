package com.example.vaadindemo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.example.vaadindemo.domain.Weapon;

public class weaponManager
{
	
	private List<Weapon> db = new ArrayList<Weapon>();
	
	public void addWeapon(Weapon weapon){
		Weapon p = new Weapon(weapon.getName(), weapon.getDescription(), weapon.getDamage(), weapon.getSpeed(),
				weapon.getCriticalChance(), weapon.getReqLevel(), weapon.getWeight(), weapon.getRarity(), weapon.getType());
		p.setId(UUID.randomUUID());
		db.add(p);
	}
	
	public List<Weapon> findAll(){
		return db;
	}

	public void delete(Weapon weapon) {
		
		Weapon toRemove = null;
		for (Weapon p: db) {
			if (p.getId().compareTo(weapon.getId()) == 0){
				toRemove = p;
				break;
			}
		}
		db.remove(toRemove);		
	}

	public void updateWeapon(Weapon weapon) {
		for (Weapon p: db){
			if (p.getId().compareTo(weapon.getId()) == 0){
				p.setName(weapon.getName());
				p.setDescription(weapon.getDescription());
				p.setDamage(weapon.getDamage());
				p.setSpeed(weapon.getSpeed());
				p.setCriticalChance(weapon.getCriticalChance());
				p.setReqLevel(weapon.getReqLevel());
				p.setWeight(weapon.getWeight());
				p.setRarity(weapon.getRarity());
				p.setType(weapon.getType());
				break;
			}
		}
		
		/*
		delete(weapon);
		addWeapon(weapon);*/
		//?? :D
	}

}
