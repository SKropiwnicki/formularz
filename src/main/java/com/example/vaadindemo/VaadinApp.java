package com.example.vaadindemo;

import com.example.vaadindemo.domain.Weapon;
import com.example.vaadindemo.service.weaponManager;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
@Theme("valo")
@Title("Weapon Database")
public class VaadinApp extends UI {

	private static final long serialVersionUID = 1L;

	private weaponManager weaponManager = new weaponManager();

	private Weapon weapon = new Weapon("Miecz", "Zwykły miecz", 100, 3.2, 10, 50, 5.5, Weapon.Rarities.NORMAL,
			Weapon.WeaponTypes.SWORD);
	private BeanItem<Weapon> weaponItem = new BeanItem<Weapon>(weapon);

	private BeanItemContainer<Weapon> weapons = new BeanItemContainer<Weapon>(
			Weapon.class);

	enum Action {
		EDIT, ADD;
	}

	private class MyFormWindow extends Window {
		private static final long serialVersionUID = 1L;

		private Action action;

		public MyFormWindow(Action act) {
			this.action = act;

			setModal(true);
			center();
			
			switch (action) {
			case ADD:
				setCaption("Dodaj nową broń");
				break;
			case EDIT:
				setCaption("Edytuj broń");
				break;
			default:
				break;
			}
			

			final FormLayout form = new FormLayout();
			final FieldGroup binder = new FieldGroup(weaponItem);

			final Button saveBtn = new Button(" Dodaj broń ");
			final Button cancelBtn = new Button(" Anuluj ");

			form.addComponent(binder.buildAndBind("Nazwa", "name"));

			TextArea area = new TextArea("Opia");
			area.setValue("");
			binder.bind(area, "description");
			form.addComponent(area);

			form.addComponent(binder.buildAndBind("Obrażenia", "damage"));
			form.addComponent(binder.buildAndBind("Szybkość ataku", "speed"));
			form.addComponent(binder.buildAndBind("Szansa na krytyk", "criticalChance"));
			form.addComponent(binder.buildAndBind("Wymagany poziom", "reqLevel"));
			form.addComponent(binder.buildAndBind("Waga", "weight"));
			form.addComponent(binder.buildAndBind("Rzadkość", "rarity", NativeSelect.class));
			form.addComponent(binder.buildAndBind("Typ", "type", NativeSelect.class));

			/*
			ComboBox combobox = new ComboBox("rarity");
			combobox.addItem("NORMAL");
			combobox.addItem("MAGIC");
			combobox.addItem("RARE");
			combobox.addItem("UNIQUE");
			combobox.addItem("LEGENDARY");
			binder.bind(combobox, "rarity");

			ComboBox comboboxtype = new ComboBox("type");
			comboboxtype.addItem("SWORD");
			comboboxtype.addItem("AXE");
			comboboxtype.addItem("BOW");
			comboboxtype.addItem("DAGGER");
			comboboxtype.addItem("STAFF");
			comboboxtype.addItem("MACE");
			comboboxtype.addItem("CROSSBOW");
			binder.bind(comboboxtype, "type");

			form.addComponent(combobox);
			form.addComponent(comboboxtype);
*/
			binder.setBuffered(true);

			binder.getField("damage").setRequired(true);
			binder.getField("name").setRequired(true);
			binder.getField("speed").setRequired(true);
			binder.getField("criticalChance").setRequired(true);
			binder.getField("reqLevel").setRequired(true);
			binder.getField("weight").setRequired(true);
			binder.getField("description").setRequired(true);
			binder.getField("rarity").setRequired(true);
			binder.getField("type").setRequired(true);
			
			binder.getField("reqLevel").addValidator(new Validator()
			{
				private static final long serialVersionUID = 1L;

				@Override
				public void validate(Object value) throws InvalidValueException 
				{
					Integer x = (Integer) value;
					if (!(x>=0 && x<=100))
					{
						throw new InvalidValueException("Poziom musi być w przedziale od 0 do 100");
					}
				}
				
			});
			
			Validator nonMinusDouble = new Validator ()
			{
				private static final long serialVersionUID = 1L;

				@Override
				public void validate(Object value) throws InvalidValueException
				{
					Double x = (Double) value;
					if(x < 0.0 )
					{
						throw new InvalidValueException("Wartość musi być większa lub równa 0");
					}
				}
			};
			
			Validator nonMinusInt = new Validator ()
			{
				private static final long serialVersionUID = 1L;

				@Override
				public void validate(Object value) throws InvalidValueException
				{
					Integer x = (Integer) value;
					if(x < 0 )
					{
						throw new InvalidValueException("Wartość musi być większa lub równa 0");
					}
				}
			};
			
			
			binder.getField("damage").addValidator(nonMinusInt);
			binder.getField("speed").addValidator(nonMinusDouble);
			binder.getField("criticalChance").addValidator(nonMinusInt);
			binder.getField("weight").addValidator(nonMinusDouble);

			VerticalLayout fvl = new VerticalLayout();
			fvl.setMargin(true);
			fvl.addComponent(form);

			HorizontalLayout hl = new HorizontalLayout();
			hl.addComponent(saveBtn);
			hl.addComponent(cancelBtn);
			fvl.addComponent(hl);

			setContent(fvl);

			saveBtn.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					try {
						binder.commit();
						
						if (action == Action.ADD) {
							weaponManager.addWeapon(weapon);
						} else if (action == Action.EDIT) {
							weaponManager.updateWeapon(weapon);
						}

						weapons.removeAllItems();
						weapons.addAll(weaponManager.findAll());
						close();
					} catch (CommitException e) {
						e.printStackTrace();
					}
				}
			});

			cancelBtn.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					binder.discard();
					close();
				}
			});
		}
	}

	final Table weaponsTable = new Table("Bronie", weapons);
	
	@Override
	protected void init(VaadinRequest request) {

		Button addPersonFormBtn = new Button("Add ");
		addPersonFormBtn.setClickShortcut(KeyCode.A);
		Button deletePersonFormBtn = new Button("Delete");
		deletePersonFormBtn.setClickShortcut(KeyCode.DELETE);
		Button editPersonFormBtn = new Button("Edit");
		editPersonFormBtn.setClickShortcut(KeyCode.E);

		VerticalLayout vl = new VerticalLayout();
		setContent(vl);

		addPersonFormBtn.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				weapon = new Weapon("Miecz", "Zwykły miecz", 100, 3.2, 10, 50, 5.5, Weapon.Rarities.NORMAL,
						Weapon.WeaponTypes.SWORD);
				weaponItem = new BeanItem<Weapon>(weapon);
				addWindow(new MyFormWindow(Action.ADD));
			}
		});

		editPersonFormBtn.addClickListener(new ClickListener() {

		
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				Weapon selectedWeapon = (Weapon) weaponsTable.getValue();
				if (selectedWeapon == null )
				{
					return;
				}
				addWindow(new MyFormWindow(Action.EDIT));
			}
		});

		deletePersonFormBtn.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				Weapon selectedWeapon = (Weapon) weaponsTable.getValue();
				if (selectedWeapon == null )
				{
					return;
				}
				if (!weapon.getName().isEmpty()) {
					weaponManager.delete(weapon);
					weapons.removeAllItems();
					weapons.addAll(weaponManager.findAll());
					weaponsTable.setValue(null);
					}
			}
		});

		HorizontalLayout hl = new HorizontalLayout();
		hl.addComponent(addPersonFormBtn);
		hl.addComponent(editPersonFormBtn);
		hl.addComponent(deletePersonFormBtn);

		
		weaponsTable.setVisibleColumns("name", "description", "damage", "speed", "criticalChance", "reqLevel",
				"weight", "rarity", "type");
		weaponsTable.setColumnHeader("name", "Nazwa");
		weaponsTable.setColumnHeader("description", "Opis");
		weaponsTable.setColumnHeader("damage", "Obrażenia");
		weaponsTable.setColumnHeader("speed", "Szybkość ataku");
		weaponsTable.setColumnHeader("criticalChance", "Szansa na krytyk");
		weaponsTable.setColumnHeader("reqLevel", "Wymagany poziom");
		weaponsTable.setColumnHeader("weight", "Waga");
		weaponsTable.setColumnHeader("rarity", "Rzadkość");
		weaponsTable.setColumnHeader("type", "Typ");
		weaponsTable.setColumnReorderingAllowed(true);
		weaponsTable.setColumnCollapsingAllowed(true);
		weaponsTable.setWidth(1300, Unit.PIXELS);
		weaponsTable.setColumnWidth("description",300);
		weaponsTable.setSelectable(true);
		weaponsTable.setImmediate(true);
	
	//	weaponsTable.setColumnExpandRatio("description", 0.2f);
		weaponsTable.setColumnAlignment("description",Table.Align.CENTER);
		weaponsTable.setColumnAlignment("name",Table.Align.CENTER);
		weaponsTable.setColumnAlignment("damage",Table.Align.CENTER);
		weaponsTable.setColumnAlignment("speed",Table.Align.CENTER);
		weaponsTable.setColumnAlignment("criticalChance",Table.Align.CENTER);
		weaponsTable.setColumnAlignment("reqLevel",Table.Align.CENTER);
		weaponsTable.setColumnAlignment("weight",Table.Align.CENTER);
		weaponsTable.setColumnAlignment("rarity",Table.Align.CENTER);
		weaponsTable.setColumnAlignment("type",Table.Align.CENTER);
		//weaponsTable.setSizeFull();

		weaponsTable.addValueChangeListener(new Property.ValueChangeListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(ValueChangeEvent event) {

				Weapon selectedWeapon = (Weapon) weaponsTable.getValue();
				if (selectedWeapon == null)
				{
					weapon.setName("");
					weapon.setDescription("");
					weapon.setDamage(0);
					weapon.setSpeed(0);
					weapon.setCriticalChance(0);
					weapon.setReqLevel(0);
					weapon.setWeight(0);
					weapon.setRarity(Weapon.Rarities.NORMAL);
					weapon.setType(Weapon.WeaponTypes.SWORD);
					weapon.setId(null);
				} else {
					weapon.setName(selectedWeapon.getName());
					weapon.setDescription(selectedWeapon.getDescription());
					weapon.setDamage(selectedWeapon.getDamage());
					weapon.setSpeed(selectedWeapon.getSpeed());
					weapon.setCriticalChance(selectedWeapon.getCriticalChance());
					weapon.setReqLevel(selectedWeapon.getReqLevel());
					weapon.setWeight(selectedWeapon.getWeight());
					weapon.setRarity(selectedWeapon.getRarity());
					weapon.setType(selectedWeapon.getType());
					weapon.setId(selectedWeapon.getId());
				}
			}
		});

		vl.addComponent(hl);
		vl.addComponent(weaponsTable);
		
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		Label label = new Label();
		horizontalLayout.addComponent(label);
		label.setValue(UI.getCurrent().toString());
		
		vl.addComponent(horizontalLayout);
	}

}
